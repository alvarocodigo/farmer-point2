from workspace2 import ma 
from workspace2.users.models import User_register

class RegisterUserSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = User_register
        load_instance = True

registeruserschema = RegisterUserSchema()