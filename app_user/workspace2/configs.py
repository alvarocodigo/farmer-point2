class BaseConfig:
    SQLALCHEMY_DATABASE_URI = 'postgres://postgres:postgres@postgres-db:5432/dbuser'
    SQLALCHEMY_TRACK_MODIFICATIONS = False 

class DevelopmentConfig(BaseConfig):
    SQLALCHEMY_ECHO = True

class ProductionConfig(BaseConfig):
    SQLALCHEMY_DATABASE_URI = 'postgres://postgres:12345678@database-1.cpijb07dvs9n.us-east-2.rds.amazonaws.com:5432/dbuser'
    SQLALCHEMY_ECHO = False