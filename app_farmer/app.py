from workspace import create_app
import os
import sys 
import coverage
import unittest

COV = None

if os.environ.get('FLASK_ENV') == 'development':
    COV = coverage.coverage(
        branch=True,
        include='workspace/*',
        omit =[
            'workspace/__init__.py'
            'workspace/_tests/*',
            'workspace/*/tests/*'
            ]
    )
    COV.start()

app = create_app()

@app.cli.command()
def test():
    tests  = unittest.TestLoader().discover('workspace',pattern='test_*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)

    if result.wasSuccessful():
        sys.exit(0)
    sys.exit(1)

@app.cli.command()
def cov():
    if COV is None:
        print('Running coverage on production environment')
        exit(0)
    tests  = unittest.TestLoader().discover('workspace',pattern='test_*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)

    if result.wasSuccessful():
        COV.stop()
        COV.save()
        COV.report()
        COV.html_report()
        COV.erase()
        sys.exit(0)
    sys.exit(1)