from workspace import db


class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    product_name = db.Column(db.String(30), nullable=False)
    product_cost_ = db.Column(db.Float, nullable=False)
    Product_history = db.Column(db.String, nullable=False)
    product_description = db.Column(db.String, nullable=False)
    created_at = db.Column(db.DateTime, default=db.func.now())
    deleted_at = db.Column(db.DateTime)
