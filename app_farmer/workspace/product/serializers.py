from workspace import ma
from workspace.product.models import Product


class ProductSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Product
        load_instance = True


product_schema = ProductSchema()
