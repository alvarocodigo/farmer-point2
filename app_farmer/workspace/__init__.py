from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_marshmallow import Marshmallow
import marshmallow
import os
from flask_cors import CORS


db = SQLAlchemy()
migrate = Migrate()
ma = Marshmallow()


def register_blueprints(app):
    from workspace.users.endpoints import register_farmer_blueprint
    from workspace.health.endpoints import health_blueprint
    from workspace.product.endpoints import product_blueprint
    app.register_blueprint(register_farmer_blueprint)
    app.register_blueprint(health_blueprint)
    app.register_blueprint(product_blueprint)


def register_error_handlers(app):
    @app.errorhandler(marshmallow.exceptions.ValidationError)
    def validation_error_handler(ex):
        return ex.messages, 400


def create_app():
    app = Flask(__name__)
    app_config = os.getenv('APP_CONFIG')
    app.config.from_object(app_config)

    CORS(app)

    db.init_app(app)
    ma.init_app(app)
    migrate.init_app(app, db)
    register_blueprints(app)
    register_error_handlers(app)
    return app
