

class BaseConfig:
    SQLALCHEMY_DATABASE_URI = 'postgres://{}:{}@{}:{}/{}'.format(
        'postgres',
        'postgres',
        'postgres-db',
        '5432',
        'dbfarmer'
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(BaseConfig):
    SQLALCHEMY_ECHO = True


class ProductionConfig(BaseConfig):
    SQLALCHEMY_DATABASE_URI = 'postgres://{}:{}@{}:{}/{}'.format(
        'postgres',
        '12345678',
        'database-1.cpijb07dvs9n.us-east-2.rds.amazonaws.com',
        '5432',
        'dbfarmer'
    )

    SQLALCHEMY_ECHO = False


class TestingConfig(BaseConfig):
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_DATABASE_URI = 'postgres://{}:{}@{}:{}/{}'.format(
        'postgres',
        'postgres',
        'postgres-db',
        '5432',
        'dbfarmer_test'
    )
