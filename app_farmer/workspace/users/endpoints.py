from flask import Blueprint, request, jsonify
from workspace import db
from workspace.users.models import Farmer_register
from workspace.users.serializers import registerfarmerschema

register_farmer_blueprint = Blueprint('resgister_farmer', __name__)


def save_farmer(user):
    db.session.add(user)
    db.session.commit()


@register_farmer_blueprint.route('/register_farmer', methods=['POST'])
def create():
    user = registerfarmerschema.load(request.get_json())
    save_farmer(user)
    return registerfarmerschema.dump(user), 201


@register_farmer_blueprint.route('/register_farmer/<id>', methods=['PUT'])
def update(id):
    user = Farmer_register.query.filter_by(id=id).first()
    if user is None:
        return 'Not found', 404
    user = registerfarmerschema.load(request.get_json(), instance=user)
    save_farmer(user)
    return registerfarmerschema.dump(user), 200


@register_farmer_blueprint.route('/register_farmer/<id>', methods=['PATCH'])
def patch(id):
    user = Farmer_register.query.filter_by(id=id).first()
    user = registerfarmerschema.load(
        request.get_json(), instance=user, partial=True
        )
    save_farmer(user)
    return registerfarmerschema.dump(user), 200


@register_farmer_blueprint.route('/register_farmer', methods=['GET'])
def list():
    users = Farmer_register.query.all()
    return jsonify(registerfarmerschema.dump(users, many=True)), 200
